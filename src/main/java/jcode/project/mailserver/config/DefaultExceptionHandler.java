package jcode.project.mailserver.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by llq on 2016/10/26.
 */
public class DefaultExceptionHandler implements HandlerExceptionResolver, Ordered {

    private Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response, Object handler, Exception e) {
        logger.error(e.getMessage(), e);
        return null;
    }

    @Override
    public int getOrder() {
        return 0;
    }

}
